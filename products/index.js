var express = require('express');
var router = express.Router();
var UserController = require('./product.controller')

router.post('/find', UserController.find)
router.post('/create', UserController.create)

module.exports = router;