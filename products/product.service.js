var MongoClient = require('mongodb').MongoClient

exports.createProduct = (data)=>{
  return new Promise((resolve, reject)=>{
    if (data){
        mongourl = "mongodb://127.0.0.1:27017";
        MongoClient.connect(mongourl, (err, client)=>{
           if(err){
            console.log('Not able to establish connection ',err);
           }
           else{
            var db = client.db('bdcdec');              
                
            db.collection('products').insert(data,(err, prod)=>{
                if(err){
                    reject()
                    console.log(err);
                }
                else{
                    console.log('Inserted into DB: ',prod);
                    resolve('Product inserted successfully')
                }
            })              
            
           
           }
        });
    }
  })
}


exports.findProducts = (data)=>{
    return new Promise((resolve, reject)=>{
        if(!data){
            reject()
        }
        else{
            mongourl = "mongodb://127.0.0.1:27017";
            MongoClient.connect(mongourl, (err, client)=>{
            if(err){
                console.log('Not able to establish connection ',err);
            }
            else{
                var db = client.db('bdcdec');
                db.collection('products').find({}).toArray( (err, prod)=>{
                    console.log('DB Op ',err,prod)
                    if(err){
                        reject()
                        client.close()
                    }
                    else{
                        if(prod){
                            resolve(prod)
                        }
                        else{
                            resolve('Products not found')
                        }
                        client.close()
                    }
                })
            
            }
            });

        }
    })
}