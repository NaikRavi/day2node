var express = require('express');
var router = express.Router();

router.use('/user', require('./userapi'))
router.use('/product', require('./products'))
router.use('/upload', require('./upload'))

module.exports = router;