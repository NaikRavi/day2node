var express = require('express')
var server = express()
var port = process.env.port || 5000;
const routes = require('./routes.js');
var BodyParser = require('body-parser');
server.use(BodyParser.json()) 
server.use(routes)

 
server.listen(port, ()=>{
    console.log('Server is running!')
})