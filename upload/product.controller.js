var userService = require('./product.service.js')

exports.create = (req, res)=>{
    userService.createProduct(req.body).then((data)=>{
        res.send(data)
    },
    (err)=>{
        res.send(err)
    })
}


exports.find = (req, res)=>{

    userService.findProducts(req.body).then((data)=>{
        res.send(data)
    },
    (err)=>{
        res.send(err)
    })
}