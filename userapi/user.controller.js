var userService = require('./user.service.js')

exports.signup = (req, res)=>{

    data = req.body;
    console.log(data);
    if(req.body){
        userService.createUser(data, (err, emp)=>{
            if(err){
                res.send({
                    error: "Internal server error"
                })
            }
            else{
                res.send({
                    message: emp
                })
            }
        });
    }
    else{
        res.send({
            message:'Email and Password are required!'
        })
    }
}


exports.login = (req, res)=>{

    userService.findUser(req.body).then((data)=>{
        res.set('auth-token',data.token)
        delete data['token']
        res.send(data)
    },
    (err)=>{
        res.send(err)
    })
}