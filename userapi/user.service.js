var MongoClient = require('mongodb').MongoClient
var Jwt = require('jsonwebtoken')

exports.createUser = (data, callback)=>{
    if (data){
        mongourl = "mongodb://127.0.0.1:27017";
        MongoClient.connect(mongourl, (err, client)=>{
           if(err){
            console.log('Not able to establish connection ',err);
           }
           else{
            var db = client.db('bdcdec');
            db.collection('employees').findOne({username:data.username}, (err, emp)=>{
                if(emp){
                    callback(null,"user already exists!")
                    client.close()
                }
                else{
                    db.collection('employees').insert(data,(err, emp)=>{
                        if(err){
                            callback(err, null)
                            console.log(err);
                        }
                        else{
                            console.log('Inserted into DB: ',emp);
                            callback(null, 'Employee inserted successfully')
                        }
                    })
                }
            })
           
           }
        });
    }
}


exports.findUser = (data)=>{
    return new Promise((resolve, reject)=>{
        if(!data){
            reject()
        }
        else{
            mongourl = "mongodb://127.0.0.1:27017";
            MongoClient.connect(mongourl, (err, client)=>{
            if(err){
                console.log('Not able to establish connection ',err);
            }
            else{
                var db = client.db('bdcdec');
                db.collection('employees').findOne(data,{fields:{username:1}}, (err, emp)=>{
                    console.log('DB Op ',err,emp)
                    if(err){
                        reject()
                        client.close()
                    }
                    else{
                        if(emp){
                            var token = Jwt.sign({user:emp.username}, 'mysecretkey')
                            console.log(token)
                            db.collection('employees').findOneAndUpdate(data, {$set:{UniqueKey:token}}, (err, updatedEmp)=>{
                                if(!err){
                                    updatedEmp.token = token
                                    console.log(updatedEmp)
                                    resolve(updatedEmp)
                                }    
                            })
                        }
                        else{
                            resolve('Invalid credentials')
                        }
                        client.close()
                    }
                })
            
            }
            });

        }
    })
}